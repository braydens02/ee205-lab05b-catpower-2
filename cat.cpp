/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file cat.cpp
///// @version 1.0
/////
///// @author Brayden Suzuki <braydens@hawaii.edu>
///// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"

double fromCatPowerToJoule( double catPower ) {
   return catPower / CATPOWER_IN_A_JOULE ;
}

double fromJouleToCatPower( double joule ) {
   return joule * CATPOWER_IN_A_JOULE ;
}
