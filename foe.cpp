/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file foe.cpp
///// @version 1.0
/////
///// @author Brayden Suzuki <braydens@hawaii.edu>
///// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "foe.h"

double fromFoesToJoule( double foe ) {
   return foe / FOES_IN_A_JOULE ;
}

double fromJouleToFoes( double joule ) {
   return joule * FOES_IN_A_JOULE ;
}
