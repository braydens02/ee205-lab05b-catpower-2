/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file foe.h
///// @version 1.0
/////
///// @author Brayden Suzuki <braydens@hawaii.edu>
///// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#pragma once

const double FOES_IN_A_JOULE = 1 / 1.0e24 ;
const char FOE = 'f';

extern double fromFoesToJoule( double foe ) ;
extern double fromJouleToFoes( double joule ) ;
