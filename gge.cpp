/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file gge.cpp
///// @version 1.0
/////
///// @author Brayden Suzuki <braydens@hawaii.edu>
///// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGgesToJoule( double gge ) {
   return gge / GGE_IN_A_JOULE ;
}

double fromJouleToGges( double joule ) {
   return joule * GGE_IN_A_JOULE ;
}
